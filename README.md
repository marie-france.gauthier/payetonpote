C'est un projet de site collaboratif réalisé avec le framework Symfony 4 et MySQL.


Ce projet a été développé à partir de maquettes HTML professionnelles.

Il comporte la bibliothèque de Stripe pour que chaques participants réalisent des paiements sécurisés grâce à une API moderne qui utilise un système anti-fraude.

MySQL workbench a été utilisé pour la création de diagramme et l'utilisation de la bdd.

https://mariefrance-gauthier.payetonpote.tp.simplon-roanne.com/