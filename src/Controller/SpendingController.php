<?php

namespace App\Controller;

use App\Entity\Spending;
use App\Entity\Campaign;
use App\Entity\Participant;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/spending")
 */
class SpendingController extends AbstractController
{

    /**
     * @Route("/spend", name="spending_new", methods="GET|POST")
     */
    public function spending_new(Request $request): Response
    {
        $campaign_id = $request->request->get('campaign_id');

        //Instancier la campagne
        $campaign = $this->getDoctrine()
                        ->getRepository(Campaign::class)
                        ->find($campaign_id);

        //Récupération des données POST enregistrement en bdd pour le participant
        $participant = new Participant();
        // $participant->setCampaignId($request->request->get('campaign_id'));
        $participant->setCampaign($campaign);
        $participant->setName($request->request->get('name'));
        $participant->setEmail($request->request->get('email'));


        //Enregistrement en base de données
        $em = $this->getDoctrine()->getManager();
        $em->persist($participant);
        $em->flush();

        $spending = new Spending();
        $spending->setAmount($request->request->get('amount') * 100);
        $spending->setLabel($request->request->get('label'));
        $spending->setParticipant($participant);

        $em = $this->getDoctrine()->getManager();
        $em->persist($spending);
        $em->flush();

        return $this->redirectToRoute('campaign_show', [
            'id' => $campaign_id
        ]);
    }

}