<?php

namespace App\Controller;

use App\Entity\Campaign;
use App\Form\CampaignType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/campaign")
 */
class CampaignController extends AbstractController
{
    /**
     * @Route("/", name="campaign_index", methods="GET")
     */
    public function index(): Response
    {
        $campaigns = $this->getDoctrine()
            ->getRepository(Campaign::class)
            ->findAll();

        return $this->render('campaign/index.html.twig', ['campaigns' => $campaigns]);
    }

    /**
     * @Route("/new", name="campaign_new", methods="GET|POST")
     */
    function new (Request $request): Response {
        $campaign = new Campaign();
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);

        //dd($_GET["campaign_title"]);
        if (isset($_GET["campaign_title"])) {
            $campaign_title = $request->query->get('campaign_title');

        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $campaign->setId();
            $em->persist($campaign);
            $em->flush();

            return $this->redirectToRoute('campaign_show', [
                'id' => $campaign->getId(),
            ]);
        }

        if (isset($_GET["campaign_title"])) {
            return $this->render('campaign/new.html.twig', [
                'campaign' => $campaign,
                'form' => $form->createView(),
                'campaign_title' => $campaign_title,
            ]);
        } else {
            return $this->render('campaign/new.html.twig', [
                'campaign' => $campaign,
                'form' => $form->createView(),
            ]);
        }
    }

    /**
     * @Route("/{id}", name="campaign_show", methods="GET")
     */
    public function show(Campaign $campaign): Response
    {
        $query = 'SELECT participant.*, payment.amount as payment_amount, spending.amount as spending_amount, spending.label as spending_label
                FROM participant 
                LEFT JOIN payment ON payment.participant_id = participant.id
                LEFT JOIN spending ON spending.participant_id = participant.id
                WHERE campaign_id = "' . $campaign->getId() . '"';

        $statement =  $this->getDoctrine()
                        ->getManager()
                        ->getConnection()
                        ->prepare($query);

        $statement->execute();

        $participantWithParticipations = $statement->fetchAll();

        $total_amount = 0;
        $total_lignes_participations = count($participantWithParticipations);

        for ($i = 0; $i <= ($total_lignes_participations - 1); $i++) {
            $total_amount += $participantWithParticipations[$i]["payment_amount"];
            $total_amount += $participantWithParticipations[$i]["spending_amount"];
        }
        $total_participation = $total_amount / 100;

        $pourcentage_goal = round(($total_participation / $campaign->getGoal()) * 100);

        return $this->render('campaign/show.html.twig', compact('campaign', 'participantWithParticipations', 'total_participation', 'total_lignes_participations', 'pourcentage_goal'));
    }

    /**
     * @Route("/{id}/edit", name="campaign_edit", methods="GET|POST")
     */
    public function edit(Request $request, Campaign $campaign): Response
    {
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('campaign_show', ['id' => $campaign->getId()]);
        }

        return $this->render('campaign/edit.html.twig', [
            'campaign' => $campaign,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="campaign_delete", methods="DELETE")
     */
    public function delete(Request $request, Campaign $campaign): Response
    {
        if ($this->isCsrfTokenValid('delete' . $campaign->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($campaign);
            $em->flush();
        }

        return $this->redirectToRoute('campaign_index');
    }

    /**
     * @Route("/{id}/pay", name="campaign_pay", methods="GET|POST")
     */
    public function pay(Request $request, Campaign $campaign): Response
    {
        //$amount_participent = $_GET['amount_participant'];
        $amount_participant = $request->query->get('amount_participant');
        //dd($amount_participent);
        return $this->render('campaign/pay.html.twig', compact('campaign', 'amount_participant'));
    }

     /**
     * @Route("/{id}/spend", name="campaign_spend", methods="GET|POST")
     */
    public function spend(Request $request, Campaign $campaign): Response
    {
        //$amount_participent = $_GET['amount_participant'];
        $amount_spend_participant = $request->query->get('amount_spend_participant');
        return $this->render('campaign/spend.html.twig', compact('campaign', 'amount_spend_participant'));
    }
}
